import styled from "styled-components";
import { Button } from "../../CommonStyled";

export const AddAnotherListContainer = styled.div`
  display: flex;
  flex-direction: column;  
  flex-wrap: wrap;
  overflow-y: auto;
  min-width: 16rem;
  align-items: center;
  background-color: ${props => props.show ? "#F1F0EE" : "transparent"};
  color: white;
  height:fit-content;  
  margin-left:0.5rem;
  gap: 0.3rem;
  border-radius: 5px;
  cursor: pointer;
  transition: 250ms ease;
`;

export const AddAnotherListButton = styled(Button)`
width: 100%;
display: ${props => !props.show ? "flex" : "none"};
background-color: ${props => props.show ? "#F1F0EE" : "transparent"};
`;



export const AddListContainer = styled(AddAnotherListContainer)`
flex-direction: column;
display: ${props =>props.show ? "flex" : "none"};


padding: 0.5rem;
gap:0.3rem;
border-radius: 5px;

`;

export const AddListWrapper = styled.div `
width:100%;
display: flex;
flex-direction: column;
gap:1rem;
padding: 0.2rem;
`;
export const ListTitleInput = styled.input`
width: 100%;
padding: 0.25rem 0.5rem;
border-radius:5px;
border:none;
outline: 1px solid #a9a9a9;
display:flex;
justify-content: flex-start;
display:inline;
&:focus{
  
}
`;
