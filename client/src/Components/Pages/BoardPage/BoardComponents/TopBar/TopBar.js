import React, { useEffect, useState } from "react";
import * as style from "./styled";
import PersonAddAltIcon from "@mui/icons-material/PersonAddAlt";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import * as common from "../../CommonStyled";
import { useDispatch, useSelector } from "react-redux";
import { boardTitleUpdate } from "../../../../../Services/boardsService";
import RightDrawer from "../../../../Drawers/RightDrawer/RightDrawer";
import BasePopover from "../../../../Modals/EditCardModal/ReUsableComponents/BasePopover";
import InviteMembers from "../../../../Modals/EditCardModal/Popovers/InviteMembers/InviteMembers";
import { deleteBoard } from "../../../../../Services/boardService";
import { useHistory } from "react-router-dom";
import HighlightOffOutlinedIcon from '@mui/icons-material/HighlightOffOutlined';
const TopBar = () => {
  const board = useSelector((state) => state.board);
  const [currentTitle, setCurrentTitle] = useState(board.title);
  const [showDrawer, setShowDrawer] = useState(false);
  const [invitePopover, setInvitePopover] = React.useState(null);
  const dispatch = useDispatch();
  useEffect(() => {
    if (!board.loading) setCurrentTitle(board.title);
  }, [board.loading, board.title]);
  const handleTitleChange = () => {
    boardTitleUpdate(currentTitle, board.id, dispatch);
  };
  const history = useHistory();
  const handleDeleteBoard = (boardId) => {
    if (boardId && dispatch) {
      deleteBoard(boardId, dispatch) // Assuming deleteBoard returns a Promise
        .then(() => {
          history.push("/boards"); // Redirect to boards home page after deletion
        })
        .catch((error) => {
          console.error("Failed to delete board:", error);
          // Optionally handle the error, e.g., show an error message to the user
        });
    } else {
      console.error(
        "handleDeleteBoard called with null or undefined boardId or dispatch"
      );
    }
  };
  return (
    <style.TopBar>
      <style.LeftWrapper>
        <style.DeleteBoardButton onClick={() => handleDeleteBoard(board.id)}>
		<HighlightOffOutlinedIcon />
          <style.TextSpan>Delete Board</style.TextSpan>
        </style.DeleteBoardButton>
        <style.InviteButton
          onClick={(event) => setInvitePopover(event.currentTarget)}
        >
          <PersonAddAltIcon />
          <style.TextSpan>Add Member</style.TextSpan>
        </style.InviteButton>
        {invitePopover && (
          <BasePopover
            anchorElement={invitePopover}
            closeCallback={() => {
              setInvitePopover(null);
            }}
            title="Invite Members"
            contents={
              <InviteMembers
                closeCallback={() => {
                  setInvitePopover(null);
                }}
              />
            }
          />
        )}

        <style.BoardNameInput
          placeholder="Board Name"
          value={currentTitle}
          onChange={(e) => setCurrentTitle(e.target.value)}
          onBlur={handleTitleChange}
        />
      </style.LeftWrapper>

      <style.RightWrapper>
        <common.Button
          onClick={() => {
            setShowDrawer(true);
          }}
        >
          <MoreHorizIcon />
          <style.TextSpan>Show menu</style.TextSpan>
        </common.Button>
      </style.RightWrapper>
      <RightDrawer
        show={showDrawer}
        closeCallback={() => {
          setShowDrawer(false);
        }}
      />
    </style.TopBar>
  );
};

export default TopBar;
